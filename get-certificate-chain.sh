#!/usr/bin/env bash
#!/usr/bin/env bash

set -Eeuo pipefail

domains=(
    "nipr.com"
    "pdb.nipr.com"
    "pdb-beta.nipr.com"
    "pdb-services.nipr.com"
    "pdb-services-beta.nipr.com"
)
port=443

get_certs () {
    local domain="$1"
    mkdir -p "$domain"
    (
        cd "$domain"
        openssl s_client \
            -showcerts \
            -verify 5 \
            -connect "$domain":"$port" \
            < /dev/null \
            | awk '/BEGIN CERTIFICATE/,/END CERTIFICATE/{ \
            if(/BEGIN CERTIFICATE/){a++}; out="cert"a".pem"; print >out}'
    )
}

validate_certs () {
    local domain="$1"
    [ -d "$domain" ] && (
        shopt -s nullglob
        local files=("$domain"/*.pem)
        [[ ${#files[@]} -eq 0 ]] \
            && echo "No certificates available to test" \
            && return
        [[ ${#files[@]} -eq 1 ]] \
            && PS4="$ " && set -x \
            && openssl verify ${files[0]}
        { set +x; } 2>/dev/null
        [[ ${#files[@]} -eq 2 ]] \
            && PS4="$ " && set -x \
            && openssl verify \
            -untrusted ${files[1]} ${files[0]}
        { set +x; } 2>/dev/null
        [[ ${#files[@]} -eq 3 ]] \
            && PS4="$ " && set -x \
            && openssl verify \
            -untrusted ${files[2]} \
            -untrusted ${files[1]} ${files[0]}
        { set +x; } 2>/dev/null
        [[ ${#files[@]} -eq 4 ]] \
            && PS4="$ " && set -x \
            && openssl verify \
            -untrusted ${files[3]} \
            -untrusted ${files[2]} \
            -untrusted ${files[1]} ${files[0]}
        { set +x; } 2>/dev/null
    )
}

get_all_certs () {
    for d in "${domains[@]}"; do
        get_certs "$d"
    done
}

validate_all_certs () {
    for d in */; do
        echo "$d"
        validate_certs "$d"
        echo
    done
}

[[ $# -eq 0 ]] \
    && (get_all_certs && validate_all_certs) \
    || (get_certs "$1" && validate_certs "$1")
